MI-MVI - Zadání semestrální práce
=============================================
Milan Vancl - vanclmil@fit.cvut.cz

*********
Úvod
*********
Minulý semestr jsem pracoval na projektu vytvoření aplikace pro klasifikaci hudebních žánrů. Na projektu jsem pracoval v rámci předmětů MI-VMM.16 a MI-PYT.
Samotná klasifikace nyní probíhá jednoduše za použití DTW algoritmu, (https://en.wikipedia.org/wiki/Dynamic_time_warping), pomocí kterého vypočítám vzdálenost časových řad vstupního vzorku od referenčních. Podle této vzdálenosti potom vypočítám pravděpodobnost příslušnosti k jednotlivým žánrům. Časové řady vzorků jsou v podobě MFCC deskriptorů (https://en.wikipedia.org/wiki/Mel-frequency_cepstrum).

*********
MI-MVI
*********
V rámci semestrální práce na předmět MI-MVI bych chtěl navázat na projekt rozšířením klasifikace žánru pomocí LSTM neuronové sítě.
Pro natrénování sítě mám k dispozici 10x100 (10 žánrů, 100 vzorků od každého) 30 sekundových vzorků hudby ve formátu .au.


*******
Data
*******
Celý dataset je možné stáhnout zde http://marsyasweb.appspot.com/download/data_sets/

*******
Pokyny ke spuštění
*******
Obsah semestrální práce je převážně v jupyter noteboocích:
 - data_preprocess.ipynb - předzpracování dat - extrakce vlastností ze surových dat + jejich uložení
 - NN_developement.ipynb - tvorba vlastní neuronové sítě
 - process_results.ipynb - zpracování výsledků učení
 Notebooky obsahují absolutní cesty, které je potřeba upravit podle svého prostředí pro jejich správné fungování.